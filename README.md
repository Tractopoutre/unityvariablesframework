### VariablesFramework for Unity (only tested with 2019.1):

Create a new 'Variable' scriptableObject with Right mouse click in the project window:
![CreateVariable](doc/CreateVariable.png)


a Variable can be referenced by script and its (current) value modified and accessed with ".Value":
```
public Vector3Variable myVector;

public void Start()
{
    myVector.Value = new Vector2(2f, 1f);
}

public void Update()
{
    Debug.Log(myVector.Value);
}
```


Current types of Variable:
* ClampedMinMaxFloatVariable
* IntVariable
* FloatVariable
* FloatListVariable
* GameObjectVariable
* Vector2Variable
* Vector3Variable
* Vector4Variable
* (experimental) VariablesContainer
* (experimental) ClampedMinMaxFloatReference


A new type of variable can be defined with a simple script:
```
using UnityEngine;


[CreateAssetMenu(fileName = "Vector3", menuName = "Variables/Vector3", order = 1)]
public class Vector3Variable : Variable<Vector3>
{
}

```
See Scripts/ScriptableObjects/Variables/*.cs for more examples




### NeatEditor:

Middle mouse button on an object field in the inspector opens its editable content in a tiny editor popup, so you don't have to browse the project, select the file and finally edit it.
![EditorPopup](doc/EditorPopup.png)

Customize how 'Variable' fields are displayed in the inspector.
![EditorSettings](doc/EditorSettings.gif)

Edit/Preferences/
.Neat.Editor (experimental)
.Neat.Variables 
