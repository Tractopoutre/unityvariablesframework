﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(ClampedMinMaxFloat))]
public class ClampedMinMaxFloatDrawer : PropertyDrawer
{
    private const float fieldWidth = 60f;

    private Rect minRect;
    private Rect maxRect;
    private Rect sliderRect;
    
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty minProp = property.FindPropertyRelative("m_min");
        SerializedProperty maxProp = property.FindPropertyRelative("m_max");
        SerializedProperty minLimitProp = property.FindPropertyRelative("m_minLimit");
        SerializedProperty maxLimitProp = property.FindPropertyRelative("m_maxLimit");

        float minLimit = minLimitProp.floatValue;
        float maxLimit = maxLimitProp.floatValue;
        
        float tempMin = minProp.floatValue;
        float tempMax = maxProp.floatValue;
        
        float cachedLabelWidth = EditorGUIUtility.labelWidth;
        
        position = EditorGUI.PrefixLabel(position, label);
        
        UpdateLayout(position);
        
        tempMin = EditorGUI.FloatField(minRect, tempMin);
        EditorGUI.MinMaxSlider(sliderRect, ref tempMin, ref tempMax, minLimit, maxLimit);
        tempMax = EditorGUI.FloatField(maxRect, tempMax);

        if (tempMin > tempMax)
            tempMin = tempMax;
        
        if (tempMax < tempMin)
            tempMax = tempMin;
        
        minProp.floatValue = Mathf.Max(tempMin, minLimit);
        maxProp.floatValue = Mathf.Min(tempMax, maxLimit);
        
        EditorGUIUtility.labelWidth = cachedLabelWidth;
    }

    private void UpdateLayout(Rect position)
    {
        minRect = new Rect(position);
        minRect.width = fieldWidth;
        
        sliderRect = new Rect(position);
        sliderRect.width -= fieldWidth * 2f;
        sliderRect.x += minRect.width;
        
        maxRect = new Rect(position);
        maxRect.width = fieldWidth;
        maxRect.x += minRect.width + sliderRect.width;
    }
}