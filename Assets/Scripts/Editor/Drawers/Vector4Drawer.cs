﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(Vector4))]
public class Vector4Drawer : PropertyDrawer
{
    private const float prefixLabelWidth = 80f;
    private const float labelWidth = 16f;
    private const float margin = 4f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float cachedLabelWidth = EditorGUIUtility.labelWidth;
    
        position = EditorGUI.PrefixLabel(position, label);
        
        EditorGUIUtility.labelWidth = labelWidth;
    
        float fieldWidth = (position.width - margin *3f) / 4f;
        position.width = fieldWidth;

        Vector4 tempValue = property.vector4Value;
        
        tempValue.x = EditorGUI.FloatField(position,"X", tempValue.x);
        position.x += fieldWidth + margin;
        
        tempValue.y = EditorGUI.FloatField(position,"Y", tempValue.y);
        position.x += fieldWidth + margin;
        
        tempValue.z = EditorGUI.FloatField(position,"Z", tempValue.z);
        position.x += fieldWidth + margin;
        
        tempValue.w = EditorGUI.FloatField(position,"W", tempValue.w);

        property.vector4Value = tempValue;
    
        EditorGUIUtility.labelWidth = cachedLabelWidth;
    }


}