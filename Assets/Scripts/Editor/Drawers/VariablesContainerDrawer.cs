﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using NeatEditor;

[CustomEditor(typeof(VariablesContainer))]
public class VariablesContainerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        VariablesContainer container = (VariablesContainer)target;
        if (GUILayout.Button("Create"))
        {
            Debug.Log(container.content.GetType());
            AddNewItem(container);
        }

        // Show default inspector property editor
        base.OnInspectorGUI();
    }


    void AddNewItem(VariablesContainer container)
    {
        FloatVariable asset = ScriptableObject.CreateInstance<FloatVariable>();
        asset.name = "FloatVariableTEST";
        var assetPath = AssetDatabase.GetAssetPath(container);
        Debug.Log(asset);
        Debug.Log(assetPath);
        AssetDatabase.AddObjectToAsset(asset, assetPath);
   //     AssetDatabase.CreateAsset(asset, assetPath);
        AssetDatabase.SaveAssets();
        

        /*
        AssetDatabase.CreateAsset(asset, "Assets/NewScripableObject.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
        */
    }
    
    
    
}