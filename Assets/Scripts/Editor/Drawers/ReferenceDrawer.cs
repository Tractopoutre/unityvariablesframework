﻿using UnityEditor;
using UnityEngine;

// Credits: Adapted from https://github.com/roboryantron/Unite2017

[CustomPropertyDrawer(typeof(Reference), true)]
public class ReferenceDrawer : PropertyDrawer 
{
    private VariablesSettings settings = VariablesPreferences.settings;
    private readonly string[] popupOptions ={ "Use Constant", "Use Variable" };
    private SerializedProperty _useConstant;

    private GUIStyle popupStyle;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) 
    {
        if (popupStyle == null) {
            popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
            popupStyle.imagePosition = ImagePosition.ImageOnly;
        }

        label = EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);

        EditorGUI.BeginChangeCheck();

        // Get properties
        _useConstant = property.FindPropertyRelative("_useConstant");
        SerializedProperty constantValue = property.FindPropertyRelative("_constantValue");
        SerializedProperty variable = property.FindPropertyRelative("_variable");

        // Calculate rect for configuration button
        Rect buttonRect = new Rect(position);
        buttonRect.yMin += popupStyle.margin.top;
        buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
        position.xMin = buttonRect.xMax;
        

        int result = EditorGUI.Popup(buttonRect, _useConstant.boolValue ? 0 : 1, popupOptions, popupStyle);
        _useConstant.boolValue = result == 0;
        
        
        // Store old indent level and set it to 0, the PrefixLabel takes care of it
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;


        EditorGUI.PropertyField(position,
            _useConstant.boolValue ? constantValue : variable,
            GUIContent.none);

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
        
        
        if (GUI.changed)
        {
            property.serializedObject.ApplyModifiedProperties();
        }
    }
    
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (_useConstant == null)
        {
            _useConstant = property.FindPropertyRelative("_useConstant");
        }
        if (!_useConstant.boolValue)
        {
            SerializedProperty variable = property.FindPropertyRelative("_variable");
            return EditorGUI.GetPropertyHeight(variable, true);
        }

        return EditorGUIUtility.singleLineHeight;
    }
}

