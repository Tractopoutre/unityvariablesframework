﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(MinMaxFloat))]
public class MinMaxFloatDrawer : PropertyDrawer
{
    private const float prefixLabelWidth = 80f;
    private const float labelWidth = 30f;
    private Rect minRect;
    private Rect maxRect;
    
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty minProp = property.FindPropertyRelative("min");
        SerializedProperty maxProp = property.FindPropertyRelative("max");

        float cachedLabelWidth = EditorGUIUtility.labelWidth;

        if ( !string.IsNullOrWhiteSpace(label.text) && label != GUIContent.none)
        {
            position = EditorGUI.PrefixLabel(position, label);
        }
        
        UpdateLayout(position);

        EditorGUIUtility.labelWidth = labelWidth;
        
        EditorGUI.PropertyField(minRect, minProp);
        EditorGUI.PropertyField(maxRect, maxProp);
        
        EditorGUIUtility.labelWidth = cachedLabelWidth;
    }

    private void UpdateLayout(Rect position)
    {
        minRect = new Rect(position);
        minRect.width = position.width /2f;
        
        maxRect = new Rect(position);
        maxRect.width = position.width /2f;
        maxRect.x += minRect.width;
    }
    
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight;
    }
}