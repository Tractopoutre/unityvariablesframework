﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(float))]
public class FloatDrawer : PropertyDrawer
{
    private const float labelWidth = 16f;
    private const float margin = 4f;
    private Rect popupRect = new Rect();
    
    private static FloatPopupWindow _floatPopupWindow = new FloatPopupWindow();
    private static Editor _editor;
    private static SerializedProperty currentProperty;
    private static bool _valueIsBeingModified;
    private static float _modfiedValue;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
    //    property.serializedObject.Update();
    //    property.floatValue = _modfiedValue;
        
        //property.serializedObject.ApplyModifiedProperties();
        //    currentProperty.serializedObject.Update();
        
        Event evt = Event.current;
        
        property.floatValue = EditorGUI.FloatField(position, label, property.floatValue);
        
        if (evt.button == 2 && position.Contains(evt.mousePosition))
        {
            currentProperty = property;
            popupRect.position = evt.mousePosition;
            PopupWindow.Show(popupRect, _floatPopupWindow);
        }
    }
/*
    public override bool CanCacheInspectorGUI(SerializedProperty property)
    {
        return false;
    }
    */
    
    
    // Preferences window
    public class FloatPopupWindow : PopupWindowContent
    {
        private int labelWidth = 150;
        private static Rect _floatModifierRect = new Rect(0,0,30,30);
        private static Event _evt;
        private static bool _holdingButton;
        private static float _startPosX;
        private static float _startValue;
        private static float _currentPosX;
        private static bool _hookedApplicationUpdate;
        private static float _currentModifier;
        private static EditorWindow _editorWindow;
        
        public override Vector2 GetWindowSize()
        {
            return new Vector2(30, 200);
        }

        public override void OnGUI(Rect rect)
        {
            _evt = Event.current;
            _floatModifierRect.position = rect.position;

            DrawFloatModifier(100f);
            DrawFloatModifier(10f);
            DrawFloatModifier(1f);
            DrawFloatModifier(.1f);
            DrawFloatModifier(.01f);
            DrawFloatModifier(.001f);
        }

        private void DrawFloatModifier(float valueModifier)
        {
            if (_evt.type == EventType.MouseDown && _evt.button == 2)
            {
                _holdingButton = true;
                _startPosX = _evt.mousePosition.x;
                _startValue = currentProperty.floatValue;

            }

            if (_evt.type == EventType.MouseDrag && _evt.button == 2)
            {
                _currentModifier = valueModifier;
                _currentPosX = _evt.mousePosition.x;
                _editorWindow = editorWindow;
                ApplyChanges();

                /*
                HookToApplicationUpdate();
                EditorApplication.update += _editorWindow.Repaint;
                EditorApplication.update.Invoke();
                */

            }
            else
            {
            //    UnHookToApplicationUpdate();
            }
            
            GUI.Label( _floatModifierRect, valueModifier.ToString());
            _floatModifierRect.y += 30;
            
            
        }

 
        
        public static void ApplyChanges()
        {
            float dif = _currentPosX - _startPosX;
            _modfiedValue= _startValue + _currentModifier * dif;
            currentProperty.floatValue = _startValue + _currentModifier * dif;
            
            currentProperty.serializedObject.ApplyModifiedProperties();
            currentProperty.serializedObject.Update();
            currentProperty.serializedObject.SetIsDifferentCacheDirty();
            //    currentProperty.serializedObject.Update();
            //   EditorUtility.SetDirty(currentProperty.serializedObject.targetObject);

            //    RepaintInspector(currentProperty.serializedObject);

            //    _editorWindow.Repaint();
            /*
            EditorApplication.update.Invoke();
            */
        }
        
        public override void OnOpen()
        {
        }

        public override void OnClose()
        {
           
        }
    }
    
    public static void RepaintInspector(SerializedObject BaseObject)
    {
        Debug.Log(BaseObject);
        foreach (var item in ActiveEditorTracker.sharedTracker.activeEditors)
        {
            
        //    Debug.Log(item);
            
            if (item.serializedObject == BaseObject)
            {
                Debug.Log("REPAINT");
                item.Repaint(); return;
            }
        }
            

    }

}