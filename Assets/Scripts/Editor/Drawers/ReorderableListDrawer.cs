﻿using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using Object = System.Object;

[CustomPropertyDrawer(typeof(System.Array), true)] 
public class ReorderableListDrawer : PropertyDrawer
{
	
    private ReorderableList _list;
    private SerializedProperty _property;
    private SerializedObject _serializedObject;

	private Rect DrawGuiField( Rect rect, SerializedProperty property, GUIContent content)
	{
		EditorGUI.PropertyField(rect, property, content);
		rect.y += EditorGUIUtility.singleLineHeight;
		return rect;
	}

    private void InitList(SerializedProperty property) 
    {
	    Debug.Log("INITLIST " + property.arraySize);
	    float lineHeight = EditorGUIUtility.singleLineHeight;
	    _property = property;
	    _serializedObject = property.serializedObject;

	    _list = new ReorderableList(_serializedObject, property, true, true, true, true);
	    _list.elementHeight = lineHeight;

	    _list.drawHeaderCallback = (Rect rect) =>
        {
	        EditorGUI.LabelField(rect, property.name);
        };

	    _list.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = _list.serializedProperty.GetArrayElementAtIndex(index);
            EditorGUI.PropertyField(rect, element);
        };
    }
 

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
	    if (_serializedObject == null)
	    {
		    InitList(property);
	    }
	    _serializedObject.Update();
	    _list.DoLayoutList();
	    _serializedObject.ApplyModifiedProperties();
    }


    /*
    
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return drawerHeight + settings.margin + 2;
    }
    */
}