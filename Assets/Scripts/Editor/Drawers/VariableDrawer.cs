﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


[CustomPropertyDrawer(typeof(Variable), true)]
public class VariableDrawer : PropertyDrawer
{
    private VariablesSettings settings
    {
        get
        {
            return VariablesPreferences.settings;
        }
    }

    private SerializedObject _serializedObject;
    private SerializedProperty _defaultValueProperty;
    private Editor _editor;
    
    private Rect displayOptionWindowRect;
    private bool optionsWindowCalled = false;
    
    private float lineHeight = EditorGUIUtility.singleLineHeight;

    private float drawerHeight = 0;
    
    private GUIContent descriptionContent = new GUIContent();
    
    private GUIContent defaultValueLabel = new GUIContent("d: ", "Default Value that is stored in the project");
    private GUIContent runtimeValueLabel = new GUIContent("r: ", "Runtime Value that is used while the game is running");

    
    private void CallOptionsWindow()
    {
        optionsWindowCalled = true;
    }

    private void ShowOptionsWindow(Event evt)
    {
        optionsWindowCalled = false;
        
        displayOptionWindowRect.x = evt.mousePosition.x;
        displayOptionWindowRect.y = evt.mousePosition.y;
        
        PopupWindow.Show(displayOptionWindowRect, new VariablesPreferencesWindow());
    }
  
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float prefixLabelWidth = position.width * settings.labelWidth / 100;
        float cachedLabelWidth = EditorGUIUtility.labelWidth;
        
        position = EditorGUI.IndentedRect(position);
        position.height -= settings.margin;
        position.y += 1;
        
        // ContextMenu
        //------------------------------------
        /*
        Event evt = Event.current;
     
        if (evt.type == EventType.MouseDown && evt.button == 1 && position.Contains(evt.mousePosition)) 
        {
            GenericMenu context = new GenericMenu ();
            context.AddItem(new GUIContent("Options"), false,CallOptionsWindow);
            context.ShowAsContext ();
        }
        
        if ( evt.type == EventType.Repaint && optionsWindowCalled)
        {
            ShowOptionsWindow(evt);
        }
        */
        //------------------------------------

        var displayOptions = settings.display;
        
        bool showObjectReference = (displayOptions & VariablesDisplayOption.ObjectReference) == VariablesDisplayOption.ObjectReference;
        bool showDefaultValue    = (displayOptions & VariablesDisplayOption.DefaultValue)    == VariablesDisplayOption.DefaultValue;
        bool showRuntimeValue    = (displayOptions & VariablesDisplayOption.RuntimeValue)    == VariablesDisplayOption.RuntimeValue;

        // Force objectReference flag to at least show something in the inspector
        if (!showObjectReference && !showDefaultValue && !showRuntimeValue)
        {
            showObjectReference = true;
            settings.display |= VariablesDisplayOption.ObjectReference;
        }

        bool variableRefIsNull = property.objectReferenceValue == null;

        if (settings.drawBoxes)
        {
            GUI.Box(position, "");
        }
        
        drawerHeight = 0;
        position.height = lineHeight;

        EditorGUIUtility.labelWidth = prefixLabelWidth;
        
        Rect labelRect  = new Rect(position);
        
        if (showObjectReference || variableRefIsNull)
        {
            Color cachedGuiColor = GUI.contentColor;
            
            if (variableRefIsNull)
            {
                GUI.contentColor = settings.errorColor;
            }
            
            EditorGUI.ObjectField(position, property);
            SetDrawerHeight(lineHeight);
            position.y += lineHeight;
            

            GUI.contentColor = cachedGuiColor;
        }
        else
        {
            labelRect.width = EditorGUIUtility.labelWidth;
        
            EditorGUI.LabelField(labelRect, property.name, EditorStyles.boldLabel);
        
            position.x += labelRect.width;
            position.width -= labelRect.width;
        }

        if (variableRefIsNull)
        {
            return;
        }


        Editor.CreateCachedEditor(property.objectReferenceValue, null ,ref _editor);



        if (_editor == null)
            return;
        
        VariablesPreferences.onSettingsChanged -= RepaintEditor; 
        VariablesPreferences.onSettingsChanged += RepaintEditor;

        /*
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel += 1;
        
        EditorGUILayout.BeginVertical("box");
        
        EditorGUI.BeginChangeCheck();
        _editor.OnInspectorGUI();
        
        bool guichanged = EditorGUI.EndChangeCheck();
        
        if (guichanged)
        {
            _editor.serializedObject.ApplyModifiedProperties();
        }
        EditorGUILayout.EndVertical();
        
        EditorGUI.indentLevel = indent;
        
        return;
        
        

        
        if (_editor != null)
        {
            VariablesPreferences.onSettingsChanged -= RepaintEditor; 
            VariablesPreferences.onSettingsChanged += RepaintEditor;
        }
        */
        
        _serializedObject = _editor.serializedObject;
        _serializedObject.Update();
        
        position.x += 20;
        position.width -= 40;
        
/*
        if (settings.descritionLines > 0)
        {
            position.height = lineHeight * settings.descritionLines;
            
            SerializedProperty descriptionProp = so.FindProperty("_description");
            
            if(settings.editableDescrition)
            position = DrawPropertyField(position, descriptionProp, GUIContent.none, settings.descriptionColor);

            position.height = lineHeight;
        }
        */
        
        EditorGUI.BeginChangeCheck();

        if (showDefaultValue)
        {
            SerializedProperty defaultValueProp = _serializedObject.FindProperty("_defaultValue");
            position.height = EditorGUI.GetPropertyHeight(defaultValueProp, true);
            
            position = DrawPropertyField(position, defaultValueProp, defaultValueLabel, settings.defaultColor);
        }

        if (showRuntimeValue)
        {
            SerializedProperty runtimeValueProp = _serializedObject.FindProperty("_currentValue");
            
            position.height = EditorGUI.GetPropertyHeight(runtimeValueProp,  true);
            position = DrawPropertyField(position, runtimeValueProp, runtimeValueLabel, settings.runtimeColor);
        }


        bool changed = EditorGUI.EndChangeCheck();
        
        if (changed)
        {
            _serializedObject.ApplyModifiedProperties();
        }
   
        EditorGUIUtility.labelWidth = cachedLabelWidth;
    }

    
    public void RepaintEditor()
    {
        if (_editor != null)
        {
            _editor.Repaint();
        }
    }
    
    private Rect DrawPropertyField(Rect position, SerializedProperty property, GUIContent label, Color color)
    {
        Rect propertyRect = new Rect(position);
        
        Color cachedBgColor = GUI.backgroundColor;
        Color cachedGuiColor = GUI.contentColor;
        
        GUI.backgroundColor = color;
        
        GUI.Box(position, "");
        
        GUI.backgroundColor = Color.Lerp(cachedBgColor, color, color.a / 2f);
        
        var cachedWidth = EditorGUIUtility.labelWidth;

        if (settings.showLabels)
        {
            EditorGUI.PropertyField(propertyRect, property, true);
        }
        else
        {
            EditorGUIUtility.labelWidth = 30;
            EditorGUI.PropertyField(propertyRect, property,label, true);
        }

        EditorGUIUtility.labelWidth = cachedWidth;
        GUI.backgroundColor = cachedBgColor;
        
        position.y += position.height;
        SetDrawerHeight(position.height);
        
        return position;
    }
    

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float height = settings.margin + 2;

        if (_editor == null || _serializedObject == null)
        {
            return EditorGUI.GetPropertyHeight(property) + height;
        }
        
        bool showObjectReference = (settings.display & VariablesDisplayOption.ObjectReference) == VariablesDisplayOption.ObjectReference;
        bool showDefaultValue    = (settings.display & VariablesDisplayOption.DefaultValue)    == VariablesDisplayOption.DefaultValue;
        bool showRuntimeValue    = (settings.display & VariablesDisplayOption.RuntimeValue)    == VariablesDisplayOption.RuntimeValue;


        if (showObjectReference && property.objectReferenceValue != null)
        {
            height += EditorGUI.GetPropertyHeight(property);
        }
        
        if (showDefaultValue)
        {
            SerializedProperty defaultValueProp = _serializedObject.FindProperty("_defaultValue");
            height += EditorGUI.GetPropertyHeight(defaultValueProp, true);
        }

        if (showRuntimeValue)
        {
            SerializedProperty runtimeValueProp = _serializedObject.FindProperty("_currentValue");
            
            height += EditorGUI.GetPropertyHeight(runtimeValueProp, true);
        }

        return height;
    }

    void SetDrawerHeight(float height)
    {
        drawerHeight += height;
    }
}