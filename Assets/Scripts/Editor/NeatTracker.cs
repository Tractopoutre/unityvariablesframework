﻿using System.Collections.Generic;

namespace NeatEditor
{
    public abstract class NeatTracker<T>
    {
        private static List<T> _allElements = new List<T>();
        public static List<T> allElements
        {
            get
            {
                return _allElements;
            }
        }

        public static void Register(T tracked)
        {
            if (_allElements.Contains(tracked))
            {
                return;
            }
  
            _allElements.Add(tracked);
        }

        public static void UnRegister(T variable)
        {
            if (_allElements.Contains(variable))
            {
                _allElements.Remove(variable);
            }
        }
    }
}
