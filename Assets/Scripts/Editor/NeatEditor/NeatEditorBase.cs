﻿// ReorderableList refs:
// http://va.lent.in/unity-make-your-lists-functional-with-reorderablelist/
// https://gist.github.com/t0chas/34afd1e4c9bc28649311
// https://medium.com/developers-writing/how-about-having-nice-arrays-and-lists-in-unity3d-by-default-e4fba13d1b50

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;
using UnityEditor.AnimatedValues;

namespace NeatEditor
{
	[CustomEditor(typeof(UnityEngine.Object), true, isFallback = true)]
	[CanEditMultipleObjects]
	public class NeatEditorBase : Editor
	{
		private bool _popupCalled;
		private Dictionary<string, ReorderableListProperty> _reorderableLists;

		private static bool _forceRepaint;
		private Rect _headerLabelRect = new Rect(0, -25, 70, 16);
		
		protected virtual void Awake()
		{
			NeatEditorPreferences.onSettingsChanged -= Repaint;
			NeatEditorPreferences.onSettingsChanged += Repaint;
			_reorderableLists = new Dictionary<string, ReorderableListProperty>(10);
		}
		
		~NeatEditorBase()
		{
			NeatEditorPreferences.onSettingsChanged -= Repaint;
			_reorderableLists.Clear();
			_reorderableLists = null;
		}

		private void DrawNeatHeader()
		{
			_headerLabelRect.width = EditorGUIUtility.currentViewWidth;
			EditorGUI.LabelField(_headerLabelRect, "Neat", EditorStyles.centeredGreyMiniLabel);
		}

		private void InitializeIfNeeded()
		{
			if (_reorderableLists == null)
			{
				_reorderableLists = new Dictionary<string, ReorderableListProperty>(10);
			}
		}
		
		public override void OnInspectorGUI()
		{
			InitializeIfNeeded();
			
			serializedObject.Update();
			if (!NeatEditorPreferences.settings.enabled)
			{
				DrawDefaultInspector();
				return;
			}

			DrawNeatHeader();
			
			DrawSerializedObject(serializedObject, this);
			
			/*
			if (HasPreviewGUI())
			{
				Rect previewRect = GUILayoutUtility.GetRect(Screen.width, Screen.width);
				DrawPreview(previewRect);
			}
*/
			serializedObject.ApplyModifiedProperties();
		}
		
		
		
		private void ShowPopupWindow(UnityEngine.Object obj, Event evt)
		{
			_popupCalled = false;
			
			Rect rect = new Rect();
			rect.x = evt.mousePosition.x;
			rect.y = evt.mousePosition.y;
        
			PopupWindow.Show(rect, new NeatEditorPopupWindow(obj, this));
		}


		
		
        
        // DrawGui with reorderable lists
        public bool DrawSerializedObject(SerializedObject serializedObject, Editor editor = null, int labelwidth = -1, bool hideScriptField = true)
        {
            Color cachedGuiColor = GUI.color;
            float cachedLabelWidth = EditorGUIUtility.labelWidth;

            if (labelwidth > -1)
            {
                EditorGUIUtility.labelWidth = labelwidth;
            }
            else if (NeatEditorPreferences.settings.labelWidth > 0)
            {
                float labelWidthFraction = Mathf.Clamp(NeatEditorPreferences.settings.labelWidth / 100f, 0f, 0.6f) ;
                EditorGUIUtility.labelWidth = EditorGUIUtility.currentViewWidth * labelWidthFraction;
            }
            
            
            serializedObject.Update();
            
            SerializedProperty property = serializedObject.GetIterator();
            int id = 0;
            bool next = property.NextVisible(true);
            if (next)
            {
                do
                {
	                if (hideScriptField && NeatEditorUtils.GetIsPropertyDefaultScript(property))
	                {
		                continue;
	                }
	                
                    //    GUI.color = cachedGuiColor;
                    DrawPropertyField(property, editor);
                    
                } while (property.NextVisible(false));
            }
            
            
            EditorGUIUtility.labelWidth = cachedLabelWidth;
            
            if (GUI.changed)
            {
                serializedObject.ApplyModifiedProperties();
                return true;
            }
            
            return false;
        }
        
        
        public void DrawPropertyField(SerializedProperty property, Editor editor = null)
        {
            if (property == null)
            {
                return;
            }
            
            bool cachedGUIEnabled         = GUI.enabled;
            bool isdefaultScriptProperty  = NeatEditorUtils.GetIsPropertyDefaultScript(property);
            bool drawArraysAsReorderable  = editor != null && NeatEditorPreferences.settings.reorderableLists;
            bool isArray                  = property.isArray && property.propertyType != SerializedPropertyType.String;

            if (isdefaultScriptProperty)
            {
                GUI.enabled = false;
            }

            //var attr = GetPropertyAttributes(property);

            if (drawArraysAsReorderable && isArray)
            {
                DrawArrayPropertyField(property, editor);           
            }
            else
            {
	            DrawSinglePropertyField(property);
            }
            
            GUI.enabled = cachedGUIEnabled;
        }

        /*
        private bool DrawPropertyArray(SerializedProperty property, bool fold)
        {
	        fold = EditorGUILayout.Foldout(fold, property.displayName);
	        if (fold)
	        {
		        SerializedProperty arraySizeProp = property.FindPropertyRelative("Array.size");
		        EditorGUILayout.PropertyField(arraySizeProp);
 
		        EditorGUI.indentLevel++;
 
		        for (int i = 0; i < arraySizeProp.intValue; i++)
		        {                
			        EditorGUILayout.PropertyField(property.GetArrayElementAtIndex(i));
		        }
 
		        EditorGUI.indentLevel--;
	        }

	        return fold;
        }
        */
        
        void DrawSinglePropertyField(SerializedProperty property)
        {
	        EditorGUILayout.PropertyField(property, true);
	            
	        // Popup editor window on middle mouse click to edit referenced objects 
	        if(property.propertyType == SerializedPropertyType.ObjectReference)
	        {
		        Event evt = Event.current;

		        if (evt.button == 2 && evt.type == EventType.MouseUp &&
		            GUILayoutUtility.GetLastRect().Contains(evt.mousePosition))
		        {
			        if (property.objectReferenceValue != null)
			        {
				        ShowPopupWindow(property.objectReferenceValue,evt);
				        evt.Use();
			        }
		        }
	        }
        }
        

/*
        void DrawPropertyFieldRecursive(SerializedProperty property, int indent = 0, bool hideScriptField = true)
        {
	        SerializedProperty iterator = property.Copy();

	        bool showChildren = false;

	        int cachedIndent = EditorGUI.indentLevel;
	        EditorGUI.indentLevel = indent;
		        
	        bool next = iterator.NextVisible(true);
	        if (next)
	        {
		        do
		        {
			        if (hideScriptField && NeatEditorUtils.GetIsPropertyDefaultScript(property))
			        {
				        continue;
			        }

			        bool isArray = property.isArray && property.propertyType != SerializedPropertyType.String;
			        
			        showChildren = EditorGUILayout.PropertyField(iterator, false);
			        
			        if (showChildren && isArray)
			        {
				        EditorGUI.indentLevel += 1;
			        }
	        
			        // Popup editor window on middle mouse click to edit referenced objects 
			        if(iterator.propertyType == SerializedPropertyType.ObjectReference)
			        {
				        Event evt = Event.current;

				        if (evt.button == 2 && evt.type == EventType.MouseUp &&
				            GUILayoutUtility.GetLastRect().Contains(evt.mousePosition))
				        {
					        if (iterator.objectReferenceValue != null)
					        {
						        ShowPopupWindow(iterator.objectReferenceValue,evt);
						        evt.Use();
					        }
				        }
			        }
			        
			        if (showChildren)
			        {
				        var recursiveIterator = iterator.Copy();
				        DrawPropertyFieldRecursive(recursiveIterator, EditorGUI.indentLevel);
			        }
			        
			        EditorGUI.indentLevel = cachedIndent;

		        } while (property.NextVisible(showChildren));
		        

	        }
	        
        }
        */
        
        
        

        public void DrawArrayPropertyField(SerializedProperty property, Editor editor)
        {
            var listProperty = GetReorderableList(property, editor);
            
            if (property.isExpanded && listProperty.list != null)
            {
                if (listProperty.list.serializedProperty != null)
                {
                    listProperty.list.DoLayoutList();
                }

            }
            else
            {
                EditorGUILayout.BeginHorizontal();

                property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, property.displayName);

                EditorGUILayout.LabelField(string.Format("size: {0}", property.arraySize));
                EditorGUILayout.EndHorizontal();
            }
        }
        
        public ReorderableListProperty GetReorderableList(SerializedProperty property, Editor editor)
        {
	        if (property == null)
	        {
		        return null;
	        }
	        
	        ReorderableListProperty listProperty = null; 

	        if (_reorderableLists.TryGetValue(property.propertyPath, out listProperty))
	        {
		        listProperty.property = property;
		        return listProperty;
	        }

	        listProperty = new ReorderableListProperty(property, editor);

	        if (_reorderableLists.ContainsKey(property.propertyPath))
	        {
		        _reorderableLists[property.propertyPath] = listProperty;
	        }
	        else
	        {
		        _reorderableLists.Add(property.propertyPath, listProperty);
	        }

	        return listProperty;
        }

	}
}