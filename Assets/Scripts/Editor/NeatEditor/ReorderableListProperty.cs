﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace NeatEditor
{
	public class ReorderableListProperty
	{
		public ReorderableList list;
		private Editor _editor;


		private string _propertyPath;
		private SerializedProperty _property;

		public SerializedProperty property
		{
			get { return _property; }
			set
			{
				_property = value;
				_propertyPath = value.propertyPath;
				
				if (list != null)
				{ 
					list.serializedProperty = value;
				}
			}
		}

	
		
		public ReorderableListProperty(SerializedProperty property, Editor editor)
		{
			_editor = editor;
			this.property = property;
			CreateList();
		}

		~ReorderableListProperty()
		{
			list = null;
		}

		
	

		private void CreateList()
		{
			list = new ReorderableList(property.serializedObject, property, true, true, true, true);

			list.drawHeaderCallback = rect =>
			{
				bool expanded = property.isExpanded;

				rect.x -= 2;
				rect.width -= EditorGUIUtility.labelWidth;

				property.isExpanded = EditorGUI.Foldout(rect, property.isExpanded, property.displayName);

				rect.x += EditorGUIUtility.labelWidth;

				EditorGUI.LabelField(rect, string.Format("size: {0}", property.arraySize));

				if (expanded != property.isExpanded)
				{
					if (_editor != null)
					{
						_editor.Repaint();
					}
				}
			};

			list.onCanRemoveCallback = (removedList) => { return list.count > 0; };
			list.drawElementCallback = DrawElement; 
			list.elementHeightCallback = (idx) =>
			{
				return Mathf.Max(EditorGUIUtility.singleLineHeight,
					       EditorGUI.GetPropertyHeight(property.GetArrayElementAtIndex(idx), GUIContent.none,
						       true)) + 4.0f;
			};
		}

		private void DrawElement(Rect rect, int index, bool active, bool focused)
		{
			if (property.GetArrayElementAtIndex(index).propertyType == SerializedPropertyType.Generic)
			{
				EditorGUI.LabelField(rect, property.GetArrayElementAtIndex(index).displayName);
			}

			rect.height = EditorGUI.GetPropertyHeight(property.GetArrayElementAtIndex(index), GUIContent.none, true);
			rect.y += 1;

			EditorGUI.PropertyField(rect, property.GetArrayElementAtIndex(index), GUIContent.none, true);

			list.elementHeight = rect.height + 4f;
		}
	}
	
	
}