﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace NeatEditor
{
    public static class NeatEditorUtils
    {
        // Helpers
        public static bool GetIsPropertyDefaultScript(SerializedProperty property)
        {
            return property.name.Equals("m_Script") &&
                   property.type.Equals("PPtr<MonoScript>") &&
                   property.propertyType == SerializedPropertyType.ObjectReference &&
                   property.propertyPath.Equals("m_Script");
        }


        public static object[] GetPropertyAttributes(SerializedProperty property)
        {
            return GetPropertyAttributes<PropertyAttribute>(property);
        }

        public static object[] GetPropertyAttributes<T>(SerializedProperty property) where T : System.Attribute
        {
            System.Reflection.BindingFlags bindingFlags =
            (
                System.Reflection.BindingFlags.GetField
                | System.Reflection.BindingFlags.GetProperty
                | System.Reflection.BindingFlags.IgnoreCase
                | System.Reflection.BindingFlags.Instance
                | System.Reflection.BindingFlags.NonPublic
                | System.Reflection.BindingFlags.Public
            );

            if (property.serializedObject.targetObject == null)
            {
                return null;
            }

            var targetType = property.serializedObject.targetObject.GetType();
            var field = targetType.GetField(property.name, bindingFlags);

            if (field != null)
            {
                return field.GetCustomAttributes(typeof(T), true);
            }

            return null;
        }
    }
}