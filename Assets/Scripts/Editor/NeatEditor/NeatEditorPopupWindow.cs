﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class NeatEditorPopupWindow : PopupWindowContent
{
    public class NeatEditorModule
    {
        public Editor editor;
        public bool isExpanded = true;
        public GUIContent labelContent;
        
        public Rect previewRect;
        
        // Make the contents of the window
        public void OnGUI()
        {
            if (labelContent == null)
            {
                System.Type targetType = editor.serializedObject.targetObject.GetType();
                Texture icon = EditorGUIUtility.ObjectContent(editor.serializedObject.targetObject, targetType).image;
                Debug.Log(icon.name);
                labelContent = new GUIContent( icon);
                labelContent.text = targetType.Name;
                //    labelContent = new GUIContent( targetType.Name);
            }
            
            editor.serializedObject.Update();
            

            isExpanded = EditorGUILayout.InspectorTitlebar(isExpanded, editor.targets);
            

            if (isExpanded)
            {
                GUILayout.BeginVertical(GUI.skin.box);
                editor.OnInspectorGUI();



                //    editor.OnPreviewSettings();
                //    previewRect.y += previewRect.height;
                //    previewRect.height = previewRect.width;
                //    editor.OnPreviewGUI(previewRect,  GUIStyle.none);
                if (editor.HasPreviewGUI() && ShaderUtil.hardwareSupportsRectRenderTexture)
                {
                    Rect lastRect = GUILayoutUtility.GetLastRect();
                    
                    
                    
                    GUILayout.BeginHorizontal();
                    GUILayout.Label( editor.GetPreviewTitle());
                    GUILayout.FlexibleSpace();
                    editor.OnPreviewSettings();
                    GUILayout.EndHorizontal();

                    float previewWidth = EditorGUIUtility.currentViewWidth - 30;
                    
                    previewRect = GUILayoutUtility.GetRect(previewWidth, previewWidth);
                    editor.OnPreviewGUI(previewRect, GUIStyle.none);
               //     editor.DrawPreview(previewRect); 
                }

                GUILayout.EndVertical();

            }
            
            editor.serializedObject.ApplyModifiedProperties();
        }


        public bool StringContains(string content, string toFind)
        {
            return CultureInfo.CurrentCulture.CompareInfo.IndexOf(content, toFind, CompareOptions.IgnoreCase) >= 0;
        }
        
        public bool GetMatchSearchPattern(string pattern)
        {
            return
            (
                StringContains(editor.target.GetType().Name, pattern)
            );
        }

        public float GetGUIHeight()
        {
            float result = EditorGUI.GetPropertyHeight(SerializedPropertyType.String, labelContent) + EditorGUIUtility.standardVerticalSpacing * 4;

            if (isExpanded)
            {
                result += editor.serializedObject.GetGUIHeight();
            }

            return result;
        }
    }
    
    
    
    public Editor parentEditor;
    public List<NeatEditorModule> modules;
    private bool justLaunched = true;

    private Vector2 scrollPosition;
    private string searchPattern = "";
    
    
    public NeatEditorPopupWindow(Object obj, Editor parentEditor)
    {
        modules = new List<NeatEditorModule>();
        List<Object> targets = new List<Object>(); 
        
        this.parentEditor = parentEditor;
        
        GameObject go = obj as GameObject;
        
        if (go != null)
        {
            var comps = go.GetComponents<Component>();

            foreach (var c in comps)
            {
                Debug.Log(c);

                Object target = c as Object;
                
                if (target != null)
                {
                    targets.Add(target);
                }
            }
        }
        else
        {
            targets.Add(obj);
        }

        foreach (var target in targets)
        {
            NeatEditorModule module = new NeatEditorModule();
            
            Editor.CreateCachedEditor(target, null, ref module.editor);
            modules.Add(module);
        }
    }
    
    public override Vector2 GetWindowSize()
    {
        float height = EditorGUIUtility.singleLineHeight * 3;
        
        foreach (var module in modules)
        {
            if (module.GetMatchSearchPattern(searchPattern))
            {
                height += module.GetGUIHeight();
            }
        }
        return new Vector2(350, Mathf.Min(height, Screen.height));
        return new Vector2(350, Mathf.Clamp(height, 200,600) );
    }
    
    public override void OnGUI(Rect position)
    {
        if (modules.Count > 0)
        {
            Object targetObj = modules[0].editor.serializedObject.targetObject;
            
            if (GUILayout.Button(targetObj.name, EditorStyles.boldLabel))
            {
                EditorGUIUtility.PingObject(targetObj);
            }
            
            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            
            Rect searchRect = GUILayoutUtility.GetRect(new GUIContent(""),EditorStyles.toolbarSearchField);
            GUI.SetNextControlName("searchFieldControl");
            searchPattern = GUI.TextField(searchRect, searchPattern, EditorStyles.toolbarSearchField);
            
            GUILayout.EndHorizontal();
        }
        
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUIStyle.none, GUI.skin.verticalScrollbar);
        GUILayout.BeginVertical();
        foreach (var module in modules)
        {
            Editor editor = module.editor;
            
            if (editor == null)
            {
                continue;
            }

            if (module.GetMatchSearchPattern(searchPattern))
            {
                module.OnGUI();
            }

        }
        GUILayout.EndVertical();
        EditorGUILayout.EndScrollView();

        if (justLaunched)
        {
            EditorGUI.FocusTextInControl("searchFieldControl");
            justLaunched = false;
        }
        
        if (GUI.changed)
        {
            parentEditor.Repaint();
         //   RefreshWindowSize();
        }
    }

    public void RefreshWindowSize()
    {
        editorWindow.maxSize = GetWindowSize();
    }
}