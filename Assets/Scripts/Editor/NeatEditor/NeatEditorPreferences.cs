﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace NeatEditor
{
    [System.Serializable]
    public class NeatEditorSettings : ScriptableObject
    {
        [SerializeField] public bool enabled = true;
        [SerializeField] public bool reorderableLists = true;
        
        [SerializeField]
        [Range(0, 50)]
        public int labelWidth = 0;
    }

    [InitializeOnLoad]
    public class NeatEditorPreferences : NeatPreferences<NeatEditorSettings>
    {
        // Display in Preferences Window
        [SettingsProvider]
        public static SettingsProvider DisplayInPreferences()
        {
            return GetSettingProvider("Neat.Editor", "Preferences/Neat.Editor", "Neat", "Editor");
        }
        
        // Display Window popup
        [MenuItem("CONTEXT/Object/NeatEditor.Preferences",false, 200)]
        private static void ShowPreferenceSettings(MenuCommand command)
        {
            PopupWindow.Show(new Rect(), new PreferencesWindow());
        }
    }
    

    // Preferences window
    public class PreferencesWindow : PopupWindowContent
    {
        private int labelWidth = 150;

        public override Vector2 GetWindowSize()
        {
            return new Vector2(300, 200);
        }

        public override void OnGUI(Rect position)
        {
            GUILayout.Label("Neat.Editor.Preferences", EditorStyles.boldLabel);
            NeatEditorPreferences.DrawDefaultGui(labelWidth);
        }

        public override void OnOpen()
        {
        }

        public override void OnClose()
        {
            NeatEditorPreferences.Save(); 
        }
    }
}
