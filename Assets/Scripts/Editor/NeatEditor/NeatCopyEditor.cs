﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NeatCopyEditor : EditorWindow
{
    string[] savedCopies = new string[5];
    bool load = false;
    
    private List<UnityEngine.Object> _copyBuffer= new List<UnityEngine.Object>();
    

    [MenuItem("Examples/Example showing systemCopyBuffer")]
    static void systemCopyBufferExample()
    {
        NeatCopyEditor window =
            EditorWindow.GetWindow<NeatCopyEditor>();
        window.Show();
    }

    
    
    void OnGUI()
    {
        load = EditorGUILayout.Toggle("Load:", load);

        EditorGUILayout.BeginHorizontal();
        for (int i = 0; i < savedCopies.Length; i++)
            if (GUILayout.Button(i.ToString()))
                if (load)
                    EditorGUIUtility.systemCopyBuffer = savedCopies[i];
                else
                    savedCopies[i] = EditorGUIUtility.systemCopyBuffer;
        EditorGUILayout.EndHorizontal();

        for (int j = 0; j < savedCopies.Length; j++)
            EditorGUILayout.LabelField("Saved " + j, savedCopies[j]);

        EditorGUILayout.LabelField("Current buffer:", EditorGUIUtility.systemCopyBuffer);
        if (GUILayout.Button("Clear all saves"))
            for (int s = 0; s < savedCopies.Length; s++)
                savedCopies[s] = "";
    }

    void OnInspectorUpdate()
    {
        this.Repaint();
    }
}