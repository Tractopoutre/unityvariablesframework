﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public static class SerializedPropertyExtension
{
    public static object GetValue(this SerializedProperty property)
    {
        System.Type parentType = property.serializedObject.targetObject.GetType();
        System.Reflection.FieldInfo fi = parentType.GetField(property.propertyPath);  
        return fi.GetValue(property.serializedObject.targetObject);
    }
    
    public static void SetValue(this SerializedProperty property,object value)
    {
        System.Type parentType = property.serializedObject.targetObject.GetType();
        System.Reflection.FieldInfo fi = parentType.GetField(property.propertyPath);//this FieldInfo contains the type.
        fi.SetValue(property.serializedObject.targetObject, value);
    }
    
    public static System.Type GetSystemType(this SerializedProperty property)
    {
        System.Type parentType = property.serializedObject.targetObject.GetType();
        System.Reflection.FieldInfo fi = parentType.GetField(property.propertyPath);
        return fi.FieldType;
    }
}
