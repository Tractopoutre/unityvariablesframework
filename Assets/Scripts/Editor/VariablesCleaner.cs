﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// ensure class initializer is called whenever scripts recompile
[InitializeOnLoadAttribute]
public static class VariablesCleaner
{
    static VariablesCleaner()
    {
        EditorApplication.playModeStateChanged += PlayModeStateChanged;
        EditorApplication.quitting += CleanAll;
    }

    private static void PlayModeStateChanged(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.ExitingPlayMode)
        {
            CleanAll();
        }
    }

    private static void CleanAll()
    {
        string debugString = VariablesTracker.allVariables.Count.ToString();
        debugString = string.Concat(debugString, "  Variables were reset:");
        
        debugString = BoldString(debugString);
        debugString = SizedString(debugString, 12);
        debugString = EndLineString(debugString);
        
        
        foreach (Variable variable in VariablesTracker.allVariables)
        {
            variable.ResetValue();

            var scriptableObj = (ScriptableObject) variable;
            
            debugString += string.Format("{0,-30}: {1,-30} {2}", BoldString(scriptableObj.name), variable.GetType(), "\n");
        }
        
        Debug.Log(debugString);
    }

    public static string BoldString(string str)
    {
        return string.Concat( "<b>", str, "</b> ");
    }
    
    public static string SizedString(string str, int size)
    {
        return string.Concat( "<size=",size ,">", str, "</size> ");
    }
    
    public static string EndLineString(string str)
    {
        return string.Concat( str, "\n");
    }
}