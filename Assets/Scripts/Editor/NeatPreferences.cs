﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace NeatEditor
{
    public abstract class NeatPreferences<T> where T : ScriptableObject
    {
        public static bool logActivity = false;
        
        protected static string prefsPath
        {
            get { return typeof(T).Name; }
        }
        
        
        public delegate void OnSettingsChangedDelegate();
        public static OnSettingsChangedDelegate onSettingsChanged; 
        
        protected static T _settings;
        public static T settings
        {
            get
            {
                if (_settings == null) 
                {
                    Load();
                }

                return _settings;
            }
            set { _settings = value; }
        }
        
        protected static SerializedObject _serializedSettings;
        public static SerializedObject serializedSettings
        {
            get
            {
                if (_serializedSettings == null)
                {
                    _serializedSettings = new SerializedObject(settings);
                }

                return _serializedSettings;
            }
        }


        public NeatPreferences()
        {
            EditorApplication.quitting += Save;
        }
        
        public static void Save()
        {
            string preferencesString = JsonUtility.ToJson(settings);
            EditorPrefs.SetString(prefsPath, preferencesString);
            
            if (logActivity)
            {
                Debug.Log((prefsPath," - Saved"));
            }
        }
	    
        public static void Load()
        {
            settings = ScriptableObject.CreateInstance<T>();

            bool loadSuccess = false;
            
            if (EditorPrefs.HasKey(prefsPath))
            {
                string preferencesString = EditorPrefs.GetString(prefsPath);

                if (!string.IsNullOrEmpty(preferencesString))
                {
                    T loadedPreferences = ScriptableObject.CreateInstance<T>();
                    JsonUtility.FromJsonOverwrite(preferencesString, loadedPreferences);

                    if (loadedPreferences != null)
                    {
                        settings = loadedPreferences;
                        loadSuccess = true; 
                    }
                }
            }
            
            SetSettingsChanged();

            if (!logActivity)
            {
                return;
            }

            if (loadSuccess)
            {
                Debug.Log(string.Concat(prefsPath," - Loaded"));
            }
            else
            {
                Debug.Log(string.Concat(prefsPath," - Failed to Load, fallback to default settings "));
            }
            
        }
        
        public static void SetSettingsChanged()
        {
            onSettingsChanged?.Invoke();
        }
        
        public static void DrawDefaultGui(int labelwidth = -1, bool showScriptField = false)
        {
            
            EditorGUI.BeginChangeCheck();

            SerializedProperty property = serializedSettings.GetIterator();
            if (property.NextVisible(true))
            {
                while (property.NextVisible(false))
                {
                    EditorGUILayout.PropertyField(property, true);
                }
            }

            bool changed = EditorGUI.EndChangeCheck();
            

            if (changed)
            {
                serializedSettings.ApplyModifiedProperties();
                Save();
                SetSettingsChanged();
            }
        }
        

        public static SettingsProvider GetSettingProvider(string theLabel, string path, params string[] keywords)
        {
            var provider = new SettingsProvider(path, SettingsScope.User)
            {
                label = theLabel,
                guiHandler = (searchContext) => { DrawDefaultGui(0); },
                keywords = new HashSet<string>(keywords)
            };

            return provider;
        }
    }
}
