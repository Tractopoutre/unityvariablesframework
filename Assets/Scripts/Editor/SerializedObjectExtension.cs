﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class SerializedObjectExtension 
{

    public static float GetGUIHeight(this SerializedObject serializedObject)
    {
        float height = 0f;
        
        SerializedProperty property = serializedObject.GetIterator();
        if (property.NextVisible(true))
        {
            do
            {
                height += EditorGUI.GetPropertyHeight(property, true);
                height += EditorGUIUtility.standardVerticalSpacing;
            } while (property.NextVisible(false));
        }

        return height;
    }
}
