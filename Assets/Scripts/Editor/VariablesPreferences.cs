﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using NeatEditor;


public class VariablesPreferences : NeatPreferences<VariablesSettings>
{
    // Display in Preferences Window
    [SettingsProvider]
    public static SettingsProvider DisplayInPreferences()
    {
        return GetSettingProvider("Neat.Variables", "Preferences/Neat.Variables", "Neat", "Variables");
    }
    
    
    // Display Window popup
    [MenuItem("CONTEXT/Object/Variables.Preferences",false, 200)]
    private static void ShowPreferenceSettings(MenuCommand command)
    {
        PopupWindow.Show(new Rect(), new VariablesPreferencesWindow());
    }
} 


// Preferences window
public class VariablesPreferencesWindow : PopupWindowContent
{
    private int labelWidth = 150;

    public override Vector2 GetWindowSize()
    {
        return new Vector2(300, 200);
    }

    public override void OnGUI(Rect position)
    {
        GUILayout.Label("Variables.Preferences", EditorStyles.boldLabel);
        VariablesPreferences.DrawDefaultGui(labelWidth);
    }

    public override void OnOpen()
    {
    }

    public override void OnClose()
    {
        VariablesPreferences.Save(); 
    }
}