﻿[System.Flags]
public enum VariablesDisplayOption
{
    ObjectReference = (1 << 0),
    DefaultValue    = (1 << 1),
    RuntimeValue    = (1 << 2),
}