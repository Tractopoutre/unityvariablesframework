﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ClampedMinMaxFloat
{
    [SerializeField] private float m_minLimit = 0f;
    [SerializeField] private float m_maxLimit = 1f;

    [SerializeField] private float m_min;
    [SerializeField] private float m_max;

    public float min
    {
        get
        {
            return m_min;
        }
        set
        {
            m_min = Mathf.Clamp(value, m_minLimit, m_maxLimit);
        }
    }
    public float max
    {
        get
        {
            return m_max;
        }
        set
        {
            m_max = Mathf.Clamp(value, m_minLimit, m_maxLimit);
        }
    }

    public ClampedMinMaxFloat(ClampedMinMaxFloat other)
    {
        if (other != null)
        {
            this.m_minLimit = other.m_minLimit;
            this.m_maxLimit = other.m_maxLimit;
    
            this.min = other.min;
            this.max = other.max;
        }
    }

    public ClampedMinMaxFloat()
    {
        this.m_minLimit = 0f;
        this.m_maxLimit = 1f;
    
        this.min = 0f;
        this.max = 1f;
    }
    
    public ClampedMinMaxFloat(float minLimit = 0f, float maxLimit = 1f)
    {
        this.m_minLimit = minLimit;
        this.m_maxLimit = maxLimit;
    
        this.min = minLimit;
        this.max = maxLimit;
    }

    public ClampedMinMaxFloat(float min, float max, float minLimit, float maxLimit)
    {
        this.m_minLimit = minLimit;
        this.m_maxLimit = maxLimit;
    
        this.min = min;
        this.max = max;
    }
}