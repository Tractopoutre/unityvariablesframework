﻿using UnityEngine;

// [CreateAssetMenu(fileName = "VariablesPreferenceSettings", menuName = "VariablesPreferenceSettings", order = 1)]

public class VariablesSettings : ScriptableObject
{
    [EnumFlag] 
    public VariablesDisplayOption display = VariablesDisplayOption.ObjectReference;
    
    [Space(10)] 
    
    [Range(10, 50)] 
    public int labelWidth = 20;

    [Range(0, 16)] 
    public int margin = 4;
    
    public bool showLabels = false;
    public bool drawBoxes = true;

    public Color defaultColor = new Color(0.09f, 1f, 0.19f, 0.2f);
    public Color runtimeColor = new Color(0.09f, 0.13f, 1f, 0.2f);
    public Color errorColor = new Color(0.84f, 0.13f, 0.13f, 1f);
    public Color descriptionColor = new Color(1f, 1f, 1f);
}