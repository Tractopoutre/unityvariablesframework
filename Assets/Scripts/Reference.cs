﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public abstract class Reference
{
}

public abstract class Reference<T> : Reference
{
    [SerializeField] protected bool _useConstant = true;
    [SerializeField] protected T _constantValue;
    public virtual Variable<T> Variable {
        get {
            return null;
        }
    }

    public Reference() { }

    public Reference(T value): this() {
        _useConstant = true;
        _constantValue = value;
    }
    
    public T Value
    {
        get
        {
            return _useConstant ? _constantValue : Variable.Value;;                
        }
    }

    public static implicit operator T(Reference<T> reference)
    {
        return reference.Value;
    }
}
