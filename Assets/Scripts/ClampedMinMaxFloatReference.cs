﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ClampedMinMaxFloatReference : Reference<ClampedMinMaxFloat>
{
    [SerializeField] private ClampedMinMaxFloatVariable _variable;
    
    protected virtual Variable<ClampedMinMaxFloat> variable 
    {
        get 
        {
            return _variable;
        }
    }
}
