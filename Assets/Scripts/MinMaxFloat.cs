﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MinMaxFloat
{
    public float min;
    public float max;
    
    public MinMaxFloat(MinMaxFloat other)
    {
        if (other != null)
        {
            this.min = other.min;
            this.max = other.max;
        }
    }
    
    public MinMaxFloat(float min, float max)
    {
        this.min = min;
        this.max = max;
    }
}
