﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VariablesTracker : ScriptableObject
{
   [SerializeField]
   private static List<Variable> _allVariables = new List<Variable>();
   
   public static List<Variable> allVariables {get { return _allVariables; }}

   public static void Register(Variable variable)
   {
      if (_allVariables.Contains(variable))
      {
         return;
      }
      
      _allVariables.Add(variable);
   }
   
   public static void UnRegister(Variable variable)
   {
      if (_allVariables.Contains(variable))
      {
         _allVariables.Remove(variable);
      }
   }
}
