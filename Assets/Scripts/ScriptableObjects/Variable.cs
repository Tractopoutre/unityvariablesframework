﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public abstract class Variable : ScriptableObject
{
    public abstract void ResetValue();
}

public abstract class Variable<T> : Variable
{
    // This used to be #if UNITY_EDITOR but there was a problem with memory layout on desktop platforms
    [TextArea(3,10)]
    [SerializeField] 
    private string _description = "";
    
    [SerializeField] 
    protected T _defaultValue;
    

    [SerializeField] 
    protected T _currentValue;

    public T Value 
    {
        get 
        {
            return _currentValue;
        }
        set 
        {
            _currentValue = value;
        }
    }

    public override void ResetValue()
    {
        _currentValue = _defaultValue;
    }

    public void OnEnable() 
    {
        ResetValue();
        
        #if UNITY_EDITOR
        VariablesTracker.Register(this);
        #endif
    }
    
    
    #if UNITY_EDITOR
    public void OnDestroy()
    {
        VariablesTracker.UnRegister(this);
    }
    #endif
    
    public static implicit operator T(Variable<T> variable) {
        if (variable == null) {
            return default(T);
        }
        return variable._currentValue;
    }
}



public abstract class VariableWithCallback<T> : Variable
{
    [TextArea(3,10)]
    [SerializeField] 
    private string _description = "";
    
    [SerializeField] 
    protected T _defaultValue;
    

    [SerializeField] 
    protected T _currentValue;
    
    public Action<T> OnValueChanged;
    
    public T Value 
    {
        get 
        {
            return _currentValue;
        }
        set 
        {
            if (OnValueChanged != null && !EqualityComparer<T>.Default.Equals(_currentValue, value)) 
            {
                OnValueChanged(value);
            }
            
            _currentValue = value;
        }
    }

    public override void ResetValue()
    {
        _currentValue = _defaultValue;
    }
    
    public void OnEnable() 
    {
        ResetValue();
        
        #if UNITY_EDITOR
        VariablesTracker.Register(this);
        #endif
    }
    
    
    #if UNITY_EDITOR
    public void OnDestroy()
    {
        VariablesTracker.UnRegister(this);
    }
    #endif
    
    // =  by value instead of by reference
    public static implicit operator T(VariableWithCallback<T> variable)
    {
        if (variable == null) {
            return default(T);
        }
        return variable._currentValue;
    }
}