﻿using System.Collections.Generic;
using UnityEngine;

public abstract class SciptableObjectContainer : ScriptableObject
{
}

public abstract class SciptableObjectContainer<T> : SciptableObjectContainer
{
    public T[] content;
}