﻿using UnityEngine;

[CreateAssetMenu(fileName = "ClampedMinMax", menuName = "Variables/ClampedMinMaxFloat", order = 1)]
public class ClampedMinMaxFloatVariable : Variable<ClampedMinMaxFloat>
{
    // override method because we want to pass values not references
    public override void ResetValue() 
    {
        _currentValue = new ClampedMinMaxFloat(_defaultValue);
    }
}
