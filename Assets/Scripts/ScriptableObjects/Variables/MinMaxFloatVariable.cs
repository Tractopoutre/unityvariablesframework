﻿using UnityEngine;

[CreateAssetMenu(fileName = "MinMaxFloat", menuName = "Variables/MinMaxFloat", order = 1)]
public class MinMaxFloatVariable : Variable<MinMaxFloat>
{
    // override method because we want to pass values not references
    public override void ResetValue() 
    {
        _currentValue = new MinMaxFloat(_defaultValue);
    }
}
