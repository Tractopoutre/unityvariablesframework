﻿using UnityEngine;


[CreateAssetMenu(fileName = "Vector4", menuName = "Variables/Vector4", order = 1)]
public class Vector4Variable : Variable<Vector4>
{
}
