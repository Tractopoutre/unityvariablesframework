﻿using UnityEngine;

[CreateAssetMenu(fileName = "Int", menuName = "Variables/Int", order = 1)]
public class IntVariable : Variable<int>
{
}
