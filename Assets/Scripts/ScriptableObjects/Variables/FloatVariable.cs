﻿using UnityEngine;

[CreateAssetMenu(fileName = "Float", menuName = "Variables/Float", order = 1)]
public class FloatVariable : Variable<float>
{
}
