﻿using UnityEngine;


[CreateAssetMenu(fileName = "FloatList", menuName = "Variables/FloatList", order = 1)]
public class FloatListVariable : Variable<float[]>
{

}