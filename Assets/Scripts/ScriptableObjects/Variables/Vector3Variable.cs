﻿using UnityEngine;


[CreateAssetMenu(fileName = "Vector3", menuName = "Variables/Vector3", order = 1)]
public class Vector3Variable : Variable<Vector3>
{
}
