﻿using UnityEngine;


[CreateAssetMenu(fileName = "GameObject", menuName = "Variables/GameObject", order = 1)]
public class GameObjectVariable : Variable<GameObject>
{
}
