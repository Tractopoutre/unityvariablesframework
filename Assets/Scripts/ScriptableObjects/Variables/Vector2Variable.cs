﻿using UnityEngine;


[CreateAssetMenu(fileName = "Vector2", menuName = "Variables/Vector2", order = 1)]
public class Vector2Variable : Variable<Vector2>
{
}