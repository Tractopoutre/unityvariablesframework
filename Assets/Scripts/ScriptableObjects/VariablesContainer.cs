﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VariablesContainer", menuName = "Variables/Container", order = 1)]
public class VariablesContainer : SciptableObjectContainer<Variable> 
{

}
