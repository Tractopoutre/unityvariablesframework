﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllTypesExample : MonoBehaviour
{
    public RenderTexture rt;
    public GameObject plopplop;
    
    public ClampedMinMaxFloatReference clamped;
    
    [Range(0, 50)]
    public float hiddenFloat;
    
    public ClampedMinMaxFloatVariable foo;

    public FloatVariable floatVar;
        
    public FloatListVariable[] floatList;
    

    public IntVariable intVar;
    

    public MinMaxFloatVariable bar;


    public Vector2Variable vectorA;
    

    public Vector3Variable vectorB;
    

    public Vector4Variable vectorC;


    public GameObjectVariable go;
    

    public GameObjectVariable[] gos;
    
    
    public List<ListOfThing> plop;

    [System.Serializable]
    public class ListOfThing
    {
        public List<GameObject> daList;
    }

}
